build:
	go build -v -o altosig ./app

build-win:
	GOOS=windows GOARHC=amd64 go build -v -o altosig-win.exe ./app

run:
	go run ./app