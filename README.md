# ALTOSig

> ALTOSig currently only supports for incoming request to Aladin exposed endpoints. ALTOSig does not support for outgoing request to ALTO (e.g. alias-management and alias-resolution).

ALTOSig will help you to generate ALTO signature.

So, you don't need to manually generate it :blush:.

## How to use

1. Copy `.env.example` to `.env`: `cp .env.example .env`
2. Set `ALADIN_API_SECRET` in `.env`
3. (optional) Set `SERVER_URL` in `.env`
4. Run the server: `make run`
5. Hit the `POST /signature` endpoint

Request:

```
curl --location --request POST 'localhost:8001/signature' \
--header 'X-Aladin-Timestamp: aladin-timestamp' \
--header 'X-Aladin-Client: biFastAlto' \
--header 'Content-Type: application/json' \
--data-raw '{
   "command":"alias-notification",
   "head":{
      "bizMessageId":"20210715FASTIDJA720H9965176271",
      "messageFrom":"FASTIDJA",
      "messageTo":"GNESIDJA"
   },
   "data":{
      "messageId":"20210715GNESIDJA72065176271",
      "dateTime":"2021-07-15T08:11:55.461",
      "receivingBic":"GNESIDJA",
      "originalMessageId":"20210715BMRIIDJA72023125192",
      "originalProxyType":"01",
      "originalProxyValue":"628187119890",
      "originalRegistrationId":"0000810305",
      "originalDisplayName":"MR. JAMES OLIVER",
      "originalBic":"GNESIDJA",
      "originalAccountNumber":"10925366963",
      "originalAccountType":"SVGS",
      "originalAccountName":"JAMES OLIVER",
      "originalCustomerType":"01",
      "originalCustomerIdNumber":"5302128167915600",
      "originalCustomerResidentStatus":"01",
      "originalCustomerTownName":"0300",
      "newRegistrationId":"0001283310",
      "newDisplayName":"MR. JAMES OLIVER",
      "newBic":"BMRIIDJA",
      "newAccountNumber":"111132587736",
      "newAccountType":"SVGS",
      "newAccountName":"JAMES OLIVER",
      "newCustomerType":"01",
      "newCustomerIdNumber":"5302128167915600",
      "newCustomerResidentStatus":"01",
      "newCustomerTownName":"0300"
   }
}'
```

Response (200 OK):

```
{
    "signature": "63e7f369efd3a802d8cd7b2b664c15111fd50ae8901011ef483757ef47d61287"
}
```

That `signature` can be used as `X-Aladin-Signature` HTTP header.

## How to run the server

 `make run`

## I don't have `make` installed, can I have the binary for mac/windows?

If you have [Go installed](https://go.dev/doc/install), you can do `go run ./app`

If you still need the binary, you can ask the maintainer.

## How to integrate this with my POSTMAN request?

1. Run the server
2. In your corresponding POSTMAN request, add the following code in the [Pre-request Script](https://learning.postman.com/docs/writing-scripts/pre-request-scripts/)

```js
const options = {
    url: "localhost:8001/signature", // Adjust with your ALTOSig server URL
    method: "POST",
    header: pm.request.getHeaders(),
    body: pm.request.body,
};

pm.sendRequest(options, function(err, response) {
    data = response.json()
    pm.request.headers.upsert({
        key: "X-Aladin-Signature",
        value: data.signature
    })
});
```

3. The script will overwrite your `X-Aladin-Signature` header with the generated signature and continue the corresponding request
