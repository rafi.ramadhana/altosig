package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/bns-engineering/common/crypto"
)

const (
	altoTimestamp        = "X-Alto-Timestamp"
	altoKey              = "X-Alto-Key"
	aladinClient         = "X-Aladin-Client"
	aladinTimestamp      = "X-Aladin-Timestamp"
	altoAPIKeyHeader     = "X-Alto-Key"
	altoTimestampHeader  = "X-Alto-Timestamp"
	aliasManagementUrl   = "/v1/bifast/alias"
	datetimeRFC3339Milli = "2006-01-02T15:04:05.000Z07:00"
)

var (
	aladinAPISecret string
	// altoAPIKey        string
	altoValidationKey string
	serverURL         string
)

func main() {
	// Read config
	viper.SetConfigName(".env")
	viper.SetConfigType("env")
	viper.AddConfigPath(".")
	viper.AddConfigPath("..")

	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
		return
	}

	aladinAPISecret = viper.GetString("ALADIN_API_SECRET")
	serverURL = viper.GetString("SERVER_URL")
	// altoAPIKey = viper.GetString("ALTO_API_KEY")
	altoValidationKey = viper.GetString("ALTO_VALIDATION_KEY")

	// Run HTTP server
	r := gin.Default()
	r.POST("/signature", generateSignature)
	r.POST("/signature/alto", generateALTOSignature)
	r.Run(serverURL)
}

func generateSignature(c *gin.Context) {
	if c.GetHeader(altoTimestamp) == "" {
		c.String(http.StatusBadRequest, "missing header: "+altoTimestamp)
		return
	}

	bBody, err := io.ReadAll(c.Request.Body)
	if err != nil {
		c.String(http.StatusInternalServerError, "failed to read body: "+err.Error())
		return
	}

	body := bBody

	if len(bBody) == 0 {
		body = make([]byte, 0)
	} else {
		tempBodyCompcat := &bytes.Buffer{}
		json.Compact(tempBodyCompcat, body)
		body = tempBodyCompcat.Bytes()
	}

	hFunc := sha256.New()
	hFunc.Write([]byte(body))

	altoKeyVal := c.GetHeader(altoKey)

	pathRel := "/v1/bifast/transfer"

	encodedReqBody := hex.EncodeToString(hFunc.Sum(nil))
	stringToSign := c.Request.Method + ":" + pathRel + ":" + altoKeyVal + ":" +
		strings.ToLower(encodedReqBody) + ":" + c.GetHeader(altoTimestamp)

	stringSignature := crypto.HMAC(sha256.New, []byte(aladinAPISecret), stringToSign)

	c.JSON(http.StatusOK, generateSignatureResponse{
		Signature: stringSignature,
	})
}

func generateALTOSignature(c *gin.Context) {
	bBody, err := io.ReadAll(c.Request.Body)
	if err != nil {
		c.String(http.StatusInternalServerError, "failed to read body: "+err.Error())
		return
	}

	altoAPIKey := c.Request.Header.Get(altoAPIKeyHeader)
	if altoAPIKey == "" {
		c.String(http.StatusInternalServerError, "missing ALTO API key in "+altoAPIKeyHeader)
		return
	}

	strTs := c.Request.Header.Get(altoTimestampHeader)
	if strTs == "" {
		c.String(http.StatusInternalServerError, "missing timestamp in "+altoTimestampHeader)
		return
	}

	ts, err := time.Parse(datetimeRFC3339Milli, strTs)
	if err != nil {
		c.String(http.StatusInternalServerError, "failed to parse time from header: "+err.Error())
		return
	}

	sig, err := generateALTOSignatureImpl(c.Request.Method, aliasManagementUrl, c.Request.Header.Get(altoAPIKeyHeader), ts, bBody)
	if err != nil {
		c.String(http.StatusInternalServerError, "failed to generate ALTO signature: "+err.Error())
		return
	}

	c.JSON(http.StatusOK, generateSignatureResponse{
		Signature: sig,
	})
}

// generateALTOSignatureImpl generates signature for ALTO's endpoints, more or less, like the one in platformbanking's alto package
func generateALTOSignatureImpl(httpMethod, relativeUrl, apiKey string, ts time.Time, bRequestBody []byte) (string, error) {
	h := sha256.New()
	h.Write(bRequestBody)

	encodedReqBody := hex.EncodeToString(h.Sum(nil))

	stringToSign := httpMethod +
		":" + relativeUrl +
		":" + apiKey +
		":" + strings.ToLower(encodedReqBody) +
		":" + ts.Format(datetimeRFC3339Milli)

	return crypto.HMAC(sha256.New, []byte(altoValidationKey), stringToSign), nil
}

type generateSignatureResponse struct {
	Signature string `json:"signature"`
}
